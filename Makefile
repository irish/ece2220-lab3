CC=gcc
CFLAGS=-Wall -Wstrict-prototypes -g -I.
DEFS=-DDEBUG
DEPS=
LDFLAGS=-lreadline
SOURCES=converter.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=converter

.c.o:
	$(CC) $(CFLAGS) $< -c -o $@

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

all: $(EXECUTABLE)

test: $(EXECUTABLE)
	./$(EXECUTABLE)

clean:
	rm -rf *.o $(EXECUTABLE)

debug:
	make clean
	$(CC) $(CFLAGS) $(DEFS) $< -c $(SOURCES)
	make all
