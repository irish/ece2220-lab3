void base4ToString(unsigned int input[4]);
void base8ToString(unsigned int input[4]);
void quadToBytes(const char *str, unsigned int result[4]);
void octToBytes(const char *str, unsigned int result[4]);
void hexToBytes(const char *str, unsigned int result[4]);
void start(void);
void getInput (unsigned int result[4]);
