/* Michael Smith <mjs3@clemson.edu>
 * prog 3 for ECE 2220. Input a Hex, Octal, or Quad number string and outputs
 * the number in the other two bases.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <readline/history.h>
#include <readline/readline.h>
#include "converter.h"

void base4ToString(unsigned int input[4]) {
	char str[128];

	const char *hex = "0123456789abcdef";
	char tmp[64];
	unsigned int i, copy[4], tmp_i;

	memcpy(copy, input, sizeof(unsigned int) * 4);

	for (i = 0, tmp_i = 0; i < 128; i += 2) {
		tmp[tmp_i++] = hex[copy[3] & 3];

		copy[3] >>= 2;
		copy[3] |= (copy[2] << 30);

		copy[2] >>= 2;
		copy[2] |= (copy[1] << 30);

		copy[1] >>= 2;
		copy[1] |= (copy[0] << 30);

		copy[0] >>= 2;
	}

	// Reverse the order of tmp[] to get the correct string
	for (i = 0; i < tmp_i; i++) {
		str[i] = tmp[tmp_i - (i + 1)];
	}
	str[i] = 0;

	printf("Quaternary: Q%s\n", str);
}

void base8ToString(unsigned int input[4]) {
	char str[128];

	const char *hex = "0123456789abcdef";
	char tmp[64];
	unsigned int i, copy[4], tmp_i;

	memcpy(copy, input, sizeof(unsigned int) * 4);

	for (i = 0, tmp_i = 0; i < 128; i += 3) {
		tmp[tmp_i++] = hex[copy[3] & 7];

		copy[3] >>= 3;
		copy[3] |= (copy[2] << 29);

		copy[2] >>= 3;
		copy[2] |= (copy[1] << 29);

		copy[1] >>= 3;
		copy[1] |= (copy[0] << 29);

		copy[0] >>= 3;
	}

	// Reverse the order of tmp[] to get the correct string
	for (i = 0; i < tmp_i; i++) {
		str[i] = tmp[tmp_i - (i + 1)];
	}
	str[i] = 0;

	printf("Octal: O%s\n", str);
}

void quadToBytes(const char *str, unsigned int result[4]) {
	const char *quad = "0123";
	unsigned int i;

	memset(result, 0, sizeof(unsigned int) * 4);

	for (i = 0; str[i] != 0; i++) {
		unsigned char nibble;
		char *ptr = strchr(quad, str[i]);

		// If it isn't a quad character, go to the next character
		if (ptr == NULL) {
			continue;
		}

		// It is a quad character
		nibble = (unsigned char) (ptr - quad);

		// Shift the result down 2 bits
		result[0] <<= 2;
		result[0] |= (result[1] >> 28);

		result[1] <<= 2;
		result[1] |= (result[2] >> 28);

		result[2] <<= 2;
		result[2] |= (result[3] >> 28);

		result[3] <<= 2;
		// and or-in the new nibble
		result[3] |= nibble;
	}
	base8ToString(result);
	printf("Hex: H%08x %08x %08x %08x\n",
			result[0], result[1], result[2], result[3]);
}

void octToBytes(const char *str, unsigned int result[4]) {
	const char *oct = "01234567";
	unsigned int i;

	memset(result, 0, sizeof(unsigned int) * 4);

	for (i = 0; str[i] != 0; i++) {
		unsigned char nibble;
		char *ptr = strchr(oct, str[i]);

		// If it isn't an oct character, go to the next character
		if (ptr == NULL) {
			continue;
		}

		// It is an oct character.
		nibble = (unsigned char) (ptr - oct);

		// Shift the result down 3 bits
		result[0] <<= 3;
		result[0] |= (result[1] >> 29);

		result[1] <<= 3;
		result[1] |= (result[2] >> 29);

		result[2] <<= 3;
		result[2] |= (result[3] >> 29);

		result[3] <<= 3;
		// and or-in the new nibble
		result[3] |= nibble;
	}

	base4ToString(result);
	printf("Hex: H%08x %08x %08x %08x\n",
			result[0], result[1], result[2], result[3]);
}

void hexToBytes(const char *str, unsigned int result[4]) {
	const char *hex = "0123456789abcdef";
	unsigned int i;

	memset(result, 0, sizeof(unsigned int) * 4);

	for (i = 0; str[i] != 0; i++) {
		unsigned char nibble;
		char *ptr = strchr(hex, tolower(str[i]));

		// If it isn't a hex character, go to the next character
		if (ptr == NULL)
			continue;

		// It is a hex character.
		nibble = (unsigned char) (ptr - hex);

		// Shift the result down 4 bits
		result[0] <<= 4;
		result[0] |= (result[1] >> 28);

		result[1] <<= 4;
		result[1] |= (result[2] >> 28);

		result[2] <<= 4;
		result[2] |= (result[3] >> 28);

		result[3] <<= 4;
		// and or-in the new nibble
		result[3] |= nibble;
	}

	base4ToString(result);
	base8ToString(result);
}

void start(void) {
	unsigned int result[4];
	while (1) {
		result[0] = 0;
		result[1] = 0;
		result[2] = 0;
		result[3] = 0;

		getInput(result);
	}
}

void getInput(unsigned int result[4]) {
	char numIn[32];
	char *line;

	line = readline("Enter a quad, octal, or hex number to be converted. "
			"(Preface your number with Q for quad, O for octal, and H for hex, "
			"'X' to exit: ");
	sscanf(line, "%s", numIn);

	switch (numIn[0]) {
		case 'Q':
		case 'q':
			quadToBytes(numIn, result);
			break;
		case 'O':
		case 'o':
			octToBytes(numIn, result);
			break;
		case 'H':
		case 'h':
			hexToBytes(numIn, result);
			break;
		case 'X':
		case 'x':
			exit(0);
		default:
			getInput(result);
	}
}

int main(int argc, char **argv) {
	start();

	return EXIT_SUCCESS;
}
